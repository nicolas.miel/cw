function pad(number) {
    return number > 9 ? number : "0"+number;
  }
  
  function humanReadable(seconds) {
    return pad(parseInt(seconds / (60*60))) + ":" +
           pad(parseInt(seconds / 60 % 60)) + ":" +
           pad(seconds % 60)
  }