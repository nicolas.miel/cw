function insertionSort(array) {
    for (let i = 1; i < array.length; i++) {
      let x = array[i];
      let j = i;
      while (j > 0 && array[j-1] > x) {
        array[j] = array[j-1];
        j--;
      }
      array[j] = x;
    }
    return array;
  }
  
  insertionSort([1, 4, 2, 8, 345, 123, 43, 32, 5643, 63, 123, 43, 2, 55, 1, 234, 92]);