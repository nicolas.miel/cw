var countBits = function(n) {
    return Number(n).toString(2).replace(/0/gi, '').length;
  };