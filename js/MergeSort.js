function mergify(a, b) {
    if (a.length == 0) {
      return b;
    }
    if (b.length == 0) {
      return a;
    }
  
    if (a[0] <= b[0]) {
      return a.slice(0,1).concat(mergify(a.slice(1, a.length), b));
    } else {
      return b.slice(0,1).concat(mergify(a, b.slice(1, b.length)));
    }
  }
  
  function mergeSort(array) {
    if (array.length <= 1) {
      return array;
    } else {
      let middle = Math.floor(array.length/2);
      let left = array.slice(0, middle);
      let right = array.slice(middle);
      return mergify(mergeSort(left), mergeSort(right));
    }
  }
  
  mergeSort([1, 4, 2, 8, 345, 123, 43, 32, 5643, 63, 123, 43, 2, 55, 1, 234, 92]);
  