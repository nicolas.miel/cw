function checkRepeats(str) {
    return /(\w)\1+/g.test(str);
  }
  
  function swap(a, b, array) {
    var tmp = array[b];
    array[b] = array[a];
    array[a] = tmp;
  }
  
  function permAlone(str) {
    let c = [];
    let array = str.split('');
    let i = 0;
    let result = 0;
    for(i = 0; i < str.length; i++) {
      c[i] = 0;
    }
  
    if (!checkRepeats(str)) {
      result += 1;
    }
    i = 0;
  
    while(i < str.length) {
      if(c[i] < i) {
          if(i%2 == 0) {
            swap(0, i, array);
          } else {
              swap(c[i], i, array);
          }
          c[i]+=1;
          i = 0;
          if (!checkRepeats(array.join(""))) {
            result += 1;
          }
      } else {
          c[i] = 0;
          i++;
      }
    }
  
    return result;
  }


  // recursive 
  
  function generateNoRepeats(n, arr, result) { 
    if (n == 1) {
      if (!checkRepeats(arr.join(""))) {
        result.push(arr.join(""));
      }
    } else {
      for (var i = 0; i < n - 1; i++) {
        generateNoRepeats(n - 1, arr, result);
        if (n%2 === 0) {
          swap (i, n - 1, arr);
        } else {
          swap (0, n - 1, arr);
        }
      }
      generateNoRepeats(n - 1, arr, result);
    }
    console.log(iteration);
    return result;
  }
  
  function permAlone(str) {
    var inputArr = str.split("");
    var result = [];
    return generateNoRepeats(inputArr.length, inputArr, result).length;
  }
  
  permAlone('aab');