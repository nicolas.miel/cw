function bubbleSort(array) {
    for(let i = array.length - 1; i > 0; i--) {
        for(let j = 0; j <= i; j++) {
            if (array[j] > array[j+1]) {
                let tmp = array[j+1];
                array[j+1] = array[j];
                array[j] = tmp;
            }
        }
    }
    return array;
}

function bubbleSortAdvance(array) {
    let sorted = true;
    for(let i = array.length - 1; i > 0; i--) {
      sorted = true;
        for(let j = 0; j <= i; j++) {
          if (array[j] > array[j+1]) {
            let tmp = array[j+1];
            array[j+1] = array[j];
            array[j] = tmp;
            sorted = false;
          }
        }
        if (sorted) {
          break;
        }
    }
    return array;
  }

  function bubbleSortJumpDown(array) {
    for(let i = array.length - 1; i > 0; i--) {
        for(let j = 0; j <= i; j++) {
            if (array[j] > array[i]) {
                let tmp = array[i];
                array[i] = array[j];
                array[j] = tmp;
            }
        }
    }
    return array;
}
  
  bubbleSort([1, 4, 2, 8, 345, 123, 43, 32, 5643, 63, 123, 43, 2, 55, 1, 234, 92]);
  