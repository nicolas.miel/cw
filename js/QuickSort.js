function swap(array, a, b) {
    let tmp = array[a];
    array[a] = array[b];
    array[b] = tmp;
  }
  
  function partition(array, first, last, pivot) {
    swap(array, last, pivot)
    let j = first;
    for (let i =first; i < last; i++) {
      if (array[i] <= array[last]) {
        swap(array, i, j);
        j++;
      }
    }
    swap(array, last, j);
    return j;
  }
  
  function recu(array, first, last) {
    if (first < last) {
      let pivot = last;
      pivot = partition(array, first, last, pivot);
      recu(array, first, pivot-1);
      recu(array, pivot+1, last)
    }
  }
  
  function quickSort(array) {
      recu(array, 0, array.length-1);
      return array;
  }
  
  // test array:
  // [1, 4, 2, 8, 345, 123, 43, 32, 5643, 63, 123, 43, 2, 55, 1, 234, 92]
  