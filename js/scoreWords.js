const array = ['a', 'b', 'c', 'd' , 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
                's','t', 'u', 'v', 'w', 'x', 'y', 'z'];

function wordScore(word) {
var score = 0;
  word.split('').forEach(x => score+= (array.indexOf(x)+1));
  return score;
}

function high(x){
  var max = 0;
  var result ='';
  x.split(' ').forEach(word => {
    var tmp = wordScore(word);
    if (tmp > max) {
      max = tmp;
      result = word;
    }
  });
  return result;
}

function highAdvancedVersion(s){
    let as = s.split(' ').map(s=>[...s].reduce((a,b)=>a+b.charCodeAt(0)-96,0));
    return s.split(' ')[as.indexOf(Math.max(...as))];
  }